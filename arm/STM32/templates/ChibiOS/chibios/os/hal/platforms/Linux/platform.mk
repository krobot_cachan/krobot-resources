# List of all the Win32 platform files.
PLATFORMSRC = ${CHIBIOS}/os/hal/platforms/Linux/hal_lld.c \
              ${CHIBIOS}/os/hal/platforms/Linux/serial_lld.c

# Required include directories
PLATFORMINC = ${CHIBIOS}/os/hal/platforms/Linux
