library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity vga is
  port(clk : in std_logic;
       hsync : out std_logic;
       vsync : out std_logic;
       active : out std_logic;
       pixel_clk : out std_logic;
       x_pos : out integer range 0 to 639;
       y_pos : out integer range 0 to 479
       );
end vga;

architecture vga of vga is
  signal internal_hsync : std_logic;
  signal internal_vsync : std_logic;
  signal internal_pixel_clk : std_logic;
  signal h_active : std_logic;
  signal v_active : std_logic;

begin

  pixel_clk <= internal_pixel_clk;
  hsync <= internal_hsync;
  vsync <= internal_vsync;
  active <= '1' when ( h_active = '1' ) and ( v_active = '1' ) else '0';

  pixel_clock: process (clk)
    variable tmp : std_logic := '0';
  begin
    if rising_edge(clk) then
      tmp := not tmp;
    end if;
    internal_pixel_clk <= tmp;
  end process pixel_clock;

  h_pixel_counter : process (internal_pixel_clk)
    constant line_length : integer := 800;
    constant pulse_length : integer := 96;
    constant front_porch : integer := 16;
    constant back_porch : integer := 48;
    variable counter : integer range 0 to line_length + 1 := 0;
    variable sync_state : std_logic := '1';
    variable h_state : std_logic := '0';
    variable x_pos_v : integer range 0 to 640 := 0;
  begin
    if (rising_edge(internal_pixel_clk))
    then
      if counter = 0 then
        sync_state := '0';
        x_pos_v := 0;
      end if;
      if counter = pulse_length then
        sync_state := '1';
      end if;

      if h_state = '1' then
        x_pos_v := x_pos_v + 1;
      end if;

      if counter = front_porch + pulse_length then
        h_state := '1';
      end if;
      if counter = line_length - back_porch then
        h_state := '0';
      end if;
      
      counter := counter + 1;
      if counter = line_length then
        counter := 0;
      end if;
    end if;
    
    internal_hsync <= sync_state;
    h_active <= h_state;
    x_pos <= x_pos_v;
  end process h_pixel_counter;

  v_pixel_counter : process (internal_hsync)
    constant line_length : integer := 521;
    constant pulse_length : integer := 2;
    constant front_porch : integer := 10;
    constant back_porch : integer := 29;
    variable counter : integer range 0 to line_length + 1 := 0;
    variable sync_state : std_logic := '1';
    variable v_state : std_logic := '0';
    variable y_pos_v : integer range 0 to 480 := 0;
  begin
    if (falling_edge(internal_hsync))
    then
      if counter = 0 then
        sync_state := '0';
        y_pos_v := 0;
      end if;
      if counter = pulse_length then
        sync_state := '1';
      end if;

      if v_state = '1' then
        y_pos_v := y_pos_v + 1;
      end if;

      if counter = pulse_length + front_porch then
        v_state := '1';
      end if;
      if counter = line_length - back_porch then
        v_state := '0';
      end if;

      counter := counter + 1;
      if counter = line_length then
        counter := 0;
      end if;
    end if;

    internal_vsync <= sync_state;
    v_active <= v_state;
    y_pos <= y_pos_v;
  end process v_pixel_counter;

end vga;
