library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity draw is
  generic (
    size_x : positive := 400;
    size_y : positive := 400;
    palet_width : positive := 5;
    palet_height : positive := 20
  );
  port (
    x_pixel : in integer range 0 to 639;
    y_pixel : in integer range 0 to 479;
    ball_x : in integer range 0 to size_x - 1;
    ball_y : in integer range 0 to size_y - 1;
    palet_l : in integer range 0 to size_y - 1;
    palet_r : in integer range 0 to size_y - 1;
    color : out std_logic_vector (7 downto 0));

end draw;

architecture draw of draw is
  type rom_type is array (0 to 3, 0 to 3) of integer range 0 to 3;

  constant left_empty : natural := palet_width * 2;
  constant up_empty : natural := 20;
  constant ball_radius : natural := 3;
  constant width_border : natural := 2;
  
  signal current_x : integer;
  signal current_y : integer;
  
  signal px : integer;
  signal py : integer;
  signal tmpx : natural;
  signal tmpy : natural;

  signal in_ball : std_logic;
  signal in_palet_l : std_logic;
  signal in_palet_r : std_logic;
  signal in_border : std_logic;
begin

  current_x <= x_pixel - left_empty;
  current_y <= y_pixel - up_empty;

  px <= ball_x;
  py <= ball_y;

  tmpx <= abs (current_x - px);
  tmpy <= abs (current_y - py);

  in_ball <= '1' when (tmpx*tmpx + tmpy*tmpy <= ball_radius*ball_radius)
                 else '0';

  in_palet_l <= '1' when current_x <= 0
                         and current_x >= -palet_width
                         and abs ( current_y - palet_l ) <= palet_height / 2
                    else '0';
  in_palet_r <= '1' when current_x >= size_x
                         and current_x <= size_x + palet_width
                         and abs ( current_y - palet_r ) <= palet_height / 2
                    else '0';

  in_border <= '1' when current_x >= 0 and current_x < size_x
                    and (     ( current_y <= 0 and current_y > -width_border )
                           or ( current_y > size_y and current_y < size_y + width_border ) )
                   else '0';

  color <= "00111000" when in_ball = '1' or in_palet_l = '1' or in_palet_r = '1' or in_border = '1'
                      else "00000000";
    
end draw;
