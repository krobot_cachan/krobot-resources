
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

package decoder is

  function decoder (
    signal val : std_logic_vector (3 downto 0))
    return std_logic_vector;

end decoder;

package body decoder is

  function decoder (signal val : std_logic_vector(3 downto 0))
    return std_logic_vector is
    variable tmp : std_logic_vector ( 6 downto 0 );
  begin
    case val is
      when "0000" => tmp := "0111111";
      when "0001" => tmp := "0000110";
      when "0010" => tmp := "1011011";
      when "0011" => tmp := "1001111";
      when "0100" => tmp := "1100110";
      when "0101" => tmp := "1101101";
      when "0110" => tmp := "1111101";
      when "0111" => tmp := "0000111";
      when "1000" => tmp := "1111111";
      when "1001" => tmp := "1101111";
      when "1010" => tmp := "1110111";
      when "1011" => tmp := "1111100";
      when "1100" => tmp := "0111001";
      when "1101" => tmp := "1011110";
      when "1110" => tmp := "1111001";
      when "1111" => tmp := "1110001";
      when others => tmp := "0000000"; -- shouldn't happen
    end case;
    return (not tmp);
  end function decoder;  

end decoder;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.decoder.ALL;

entity simple_7seg is
  port(clk : in  std_logic;
       value : in std_logic_vector (15 downto 0);
       pti : in std_logic_vector(3 downto 0);
       seg : out std_logic_vector(6 downto 0);
       pto : out std_logic;
       an : out std_logic_vector(3 downto 0) );
end simple_7seg;

architecture simple_7seg of simple_7seg is
signal selected_value : std_logic_vector ( 3 downto 0 );
begin

seg <= decoder(selected_value);
  
process (clk)
  variable state : std_logic_vector (3 downto 0) := "0111";
  variable out_value : std_logic_vector (3 downto 0) := "0000";
  variable out_pt : std_logic;
begin
  if rising_edge(clk) then
    case state is
      when "0111" =>
        state := "1011";
        out_value := value (15 downto 12);
        out_pt := pti(3);
      when "1011" =>
        state := "1101";
        out_value := value (11 downto 8);
        out_pt := pti(2);
      when "1101" =>
        state := "1110";
        out_value := value (7 downto 4);
        out_pt := pti(1);
      when others =>
        state := "0111";
        out_value := value (3 downto 0);
        out_pt := pti(0);
    end case;
  end if;
  an <= state;
  selected_value <= out_value;
  pto <= not out_pt;
end process;
  
end simple_7seg;
