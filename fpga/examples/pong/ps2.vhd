library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity driver_ps2 is
    port (clk : in std_logic;
          ps2d : in std_logic;
          ps2c : in std_logic;
          data : out std_logic_vector ( 0 to 7 );
          int : out std_logic );
end entity driver_ps2;

architecture driver_ps2 of driver_ps2 is
type state_type is ( bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7,
                     bit8, bit9, end_state, error_state );
signal tmp : std_logic_vector ( 8 downto 1 ) := "00000000";
signal state : state_type := bit0;
signal int_l : std_logic := '0';
signal data_l : std_logic_vector ( 0 to 7 ) := "00000000";
signal ps2_clk : std_logic := '1';
signal ps2_data : std_logic := '1';
begin

process (clk)
begin
  if (rising_edge(clk)) then
    ps2_clk <= ps2c;
  end if;
end process;

process (clk)
begin
  if (rising_edge(clk)) then
    ps2_data <= ps2d;
  end if;
end process;

int <= int_l;
data <= data_l;

process (ps2_clk)
begin
  if (falling_edge(ps2_clk))
  then
    case state is
      when bit0 =>
        if ps2_data = '0'
        then state <= bit1;
        else state <= error_state;
        end if;
      when bit1 =>
        state <=  bit2;
        tmp(1) <= ps2_data;
      when bit2 =>
        state <=  bit3;
        tmp(2) <= ps2_data;
      when bit3 =>
        state <=  bit4;
        tmp(3) <= ps2_data;
      when bit4 =>
        state <=  bit5;
        tmp(4) <= ps2_data;
      when bit5 =>
        state <=  bit6;
        tmp(5) <= ps2_data;
      when bit6 =>
        state <=  bit7;
        tmp(6) <= ps2_data;
      when bit7 =>
        state <=  bit8;
        tmp(7) <= ps2_data;
      when bit8 =>
        state <= bit9;
        tmp(8) <= ps2_data;
      when bit9 =>
        state <= end_state;
        -- tmp(9) <= ps2_data;
        -- should verify parity
        int_l <= '1';
        data_l <= tmp(8 downto 1);
      when end_state =>
        int_l <= '0';
        if ps2_data = '1'
        then state <= bit0;
        else state <= error_state;
        end if;
      when error_state =>
        state <= error_state;
        -- should make something to recover
    end case;

  end if;
end process;

end driver_ps2;
