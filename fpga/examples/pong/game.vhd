library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity game is
    generic (
      size_x : positive := 400;
      size_y : positive := 400;
      palet_width : positive := 5;
      palet_height : positive := 20);
    port (
    clk : in std_logic;
    key_up_l : in std_logic;
    key_down_l : in std_logic;
    key_up_r : in std_logic;
    key_down_r : in std_logic;
    ball_x : out integer range 0 to size_x - 1;
    ball_y : out integer range 0 to size_y - 1;
    palet_l : out integer range 0 to size_y - 1;
    palet_r : out integer range 0 to size_y - 1;
    score_l : out integer range 0 to 255;
    score_r : out integer range 0 to 255
--    keys : out std_logic_vector ( 3 downto 0 )
    );
end entity game;

architecture game of game is

  constant init_x : integer := size_x / 2;
  constant init_y : integer := size_y / 3;

  signal game_clk : std_logic;
  --signal key_up : std_logic;
  --signal key_down : std_logic;
  --signal key_left : std_logic;
  --signal key_right : std_logic;
  signal px : integer range 0 to size_x - 1;
  signal py : integer range 0 to size_y - 1;
begin

--  keys <= key_up & key_down & key_left & key_right;

  ball_x <= px;
  ball_y <= py;

  --process (key_int)
  --  variable up : std_logic := '0';
  --  variable down : std_logic := '0';
  --  variable left_dir : std_logic := '0';
  --  variable right_dir : std_logic := '0';
  --begin
  --  if rising_edge(key_int) then
  --    if (up_key = '0' and scan_code = "0111"&"0101" ) then
  --      up := '1';
  --    end if;
  --    if (up_key = '1' and scan_code = "0111"&"0101" ) then
  --      up := '0';
  --    end if;
  --    if (up_key = '0' and scan_code = "0111"&"0010" ) then
  --      down := '1';
  --    end if;
  --    if (up_key = '1' and scan_code = "0111"&"0010" ) then
  --      down := '0';
  --    end if;
  --    if (up_key = '0' and scan_code = "0111"&"0100" ) then
  --      right_dir := '1';
  --    end if;
  --    if (up_key = '1' and scan_code = "0111"&"0100" ) then
  --      right_dir := '0';
  --    end if;
  --    if (up_key = '0' and scan_code = "0110"&"1011" ) then
  --      left_dir := '1';
  --    end if;
  --    if (up_key = '1' and scan_code = "0110"&"1011" ) then
  --      left_dir := '0';
  --    end if;
  --  end if;

  --  key_up <= up;
  --  key_down <= down;
  --  key_left <= left_dir;
  --  key_right <= right_dir;
    
  --end process;


  process (clk)
    constant length : integer := 1_000_000;
    variable counter : integer range 0 to length:= 0;
    variable tmp : std_logic := '0';
  begin
    if rising_edge(clk) then
      counter := counter + 1;
      if counter = length / 2 then
        tmp := not tmp;
        counter := 0;
      end if;
    end if;
    game_clk <= tmp;
  end process;

  update_process: process (game_clk)
    variable x : integer := 50;
    variable y : integer := 100;
    variable v_x : integer := 2;
    variable v_y : integer := 2;
    variable y_l : integer := 100;
    variable y_r : integer := 100;
    variable v_score_l : integer range 0 to 255 := 0;
    variable v_score_r : integer range 0 to 255 := 0;

  begin
    if rising_edge(game_clk) then

      -- begin bounce on palet
      if    x >= size_x-1
        and v_x > 0
        and abs ( y - y_r ) < palet_height
      then
        v_x := -v_x;
      end if;
      if    x <= 0
        and v_x < 0
        and abs ( y - y_l ) < palet_height
      then
        v_x := -v_x;
      end if;
      -- end bounce on palet

      -- begin bounce on up wall
      if (  (y >= size_y-1 and v_y > 0)
         or (y <= 0 and v_y < 0) )
      then
        v_y := -v_y;
      end if;
      -- end bounce on up wall

      -- begin move ball
      x := x + v_x;
      y := y + v_y;
      -- end move ball

      -- begin loose
      if x >= size_x + palet_width
      then
        v_score_l := 1 + v_score_l;
        x := init_x;
        y := init_y;
      end if;
      if x < 0 - palet_width
      then
        v_score_r := 1 + v_score_r;
        x := init_x;
        y := init_y;
      end if;
      -- end loose


      -- begin move palet
      if key_up_l = '1' and y_l > palet_height then
        y_l := y_l - 1;
      end if;
      if key_down_l = '1' and y_l < size_y - palet_height then
        y_l := y_l + 1;
      end if;
      if key_up_r = '1' and y_r > palet_height then
        y_r := y_r - 1;
      end if;
      if key_down_r = '1' and y_r < size_y - palet_height then
        y_r := y_r + 1;
      end if;
      -- end move palet

    end if;

    palet_l <= y_l;
    palet_r <= y_r; 

    px <= x;
    py <= y;

    score_l <= v_score_l;
    score_r <= v_score_r;

  end process update_process;

  
end architecture;
