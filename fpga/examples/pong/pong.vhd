library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity pong is
    port (
    clk : in std_logic;

    btn : in  std_logic_vector(3 downto 0);
    Led : out std_logic_vector(7 downto 0);
--    usb : in std_logic;
--    sw : in std_logic_vector(7 downto 0);

    seg : out std_logic_vector(6 downto 0);
    dp : out std_logic;
    an : out std_logic_vector(3 downto 0);

    ps2c : in std_logic;
    ps2d : in std_logic;

    vga_out : out std_logic_vector(7 downto 0);
    hsync : out std_logic;
    vsync : out std_logic
    );
end entity pong;

architecture pong of pong is

  component draw is
    generic (
      size_x : positive := 400;
      size_y : positive := 400;
      palet_width : positive := 5;
      palet_height : positive := 20      
      );
    port (
--      update : in std_logic;
      x_pixel : in  integer range 0 to 639;
      y_pixel : in  integer range 0 to 479;
      ball_x : in integer range 0 to size_x - 1;
      ball_y : in integer range 0 to size_y - 1;
      palet_l : in integer range 0 to size_y - 1;
      palet_r : in integer range 0 to size_y - 1;
      color : out std_logic_vector (7 downto 0));
  end component;

  component vga is
  port(clk : in std_logic;
       hsync : out std_logic;
       vsync : out std_logic;
       active : out std_logic;
       pixel_clk : out std_logic;
       x_pos : out integer range 0 to 639;
       y_pos : out integer range 0 to 479
       );
  end component;

  component driver_ps2 is
    port (clk : in std_logic;
          ps2d : in std_logic;
          ps2c : in std_logic;
          data : out std_logic_vector ( 0 to 7 );
          int : out std_logic );
  end component;

  component keyboard is
    port (clk : in std_logic;
          data_in : in std_logic_vector ( 0 to 7 );
          int_in : in std_logic;
          extended : out std_logic;
          up : out std_logic;
          code : out std_logic_vector ( 0 to 7 );
          int : out std_logic );
  end component;

  component simple_7seg is
    port(clk : in  std_logic;
         value : in std_logic_vector (15 downto 0);
         pti : in std_logic_vector(3 downto 0);
         seg : out std_logic_vector(6 downto 0);
         pto : out std_logic;
         an : out std_logic_vector(3 downto 0) );
  end component;

  component game is
    generic (
      size_x : positive := 400;
      size_y : positive := 400;
      palet_width : positive := 5;
      palet_height : positive := 20);
    port (
    clk : in std_logic;
    key_up_l : in std_logic;
    key_down_l : in std_logic;
    key_up_r : in std_logic;
    key_down_r : in std_logic;
    ball_x : out integer range 0 to size_x - 1;
    ball_y : out integer range 0 to size_y - 1;
    palet_l : out integer range 0 to size_y - 1;
    palet_r : out integer range 0 to size_y - 1;
    score_l : out integer range 0 to 255;
    score_r : out integer range 0 to 255
--    keys : out std_logic_vector ( 3 downto 0 )
    );
  end component;

  constant size_x : integer := 400;
  constant size_y : integer := 400;
  constant palet_width : positive := 5;
  constant palet_height : positive := 20;

  signal vga_color : std_logic_vector ( 7 downto 0 );
  signal internal_vsync : std_logic;
  signal vga_x : integer range 0 to 639;
  signal vga_y : integer range 0 to 479;

  signal active_output : std_logic;
  signal pixel_clk : std_logic;

-- ps2 keyboard signals
  signal ps2_data : std_logic_vector ( 7 downto 0 );
  signal scan_code : std_logic_vector ( 7 downto 0 );
  signal ps2_int : std_logic;
  signal key_up : std_logic;
  signal key_extended : std_logic;
  signal key_int : std_logic;

  signal key_up_l : std_logic;
  signal key_down_l : std_logic;
  signal key_up_r : std_logic;
  signal key_down_r : std_logic;
  
  signal slow_clk : std_logic;
  signal value_7seg : std_logic_vector ( 15 downto 0 );
  signal point_7seg : std_logic_vector ( 3 downto 0 );

-- game data
  signal ball_x : integer range 0 to size_x - 1;
  signal ball_y : integer range 0 to size_y - 1;

  signal palet_l : integer range 0 to size_y - 1;
  signal palet_r : integer range 0 to size_y - 1;
  
  signal score_l : integer range 0 to 255 := 0;
  signal score_r : integer range 0 to 255 := 0;
  signal score_l_vector : std_logic_vector ( 7 downto 0 );
  signal score_r_vector : std_logic_vector ( 7 downto 0 );
  
begin

  vsync <= internal_vsync;
  vga_out <= vga_color when active_output = '1' else "00000000";

  score_l_vector <= (conv_std_logic_vector(score_l,8));
  score_r_vector <= (conv_std_logic_vector(score_r,8));
  value_7seg <=   (score_l_vector(3 downto 0)) & (score_l_vector(7 downto 4))
                & (score_r_vector(3 downto 0)) & (score_r_vector(7 downto 4));
  key_up_l <= btn(0);
  key_down_l <= btn(1);
  key_up_r <= btn(2);
  key_down_r <= btn(3);
  
  draw_1 : draw
    generic map
    (
      size_x,
      size_y,
      palet_width,
      palet_height
      )
    port map
    ( -- internal_vsync,
      vga_x,
      vga_y,
      ball_x,
      ball_y,
      palet_l,
      palet_r,
      vga_color
      );

  game_1 : game
    generic map (
      size_x,
      size_y,
      palet_width,
      palet_height)
    port map (
    clk,
    key_up_l,
    key_down_l,
    key_up_r,
    key_down_r,
    ball_x,
    ball_y,
    palet_l,
    palet_r,
    score_l,
    score_r
--    led ( 3 downto 0 )
    );

  led ( 7 downto 0 ) <= "00000000";

  vga_1 : vga port map
    ( clk,
      hsync,
      internal_vsync,
      active_output,
      pixel_clk,
      vga_x,
      vga_y
      );

  driver_ps2_1 : driver_ps2 port map
    ( clk,
      ps2d,
      ps2c,
      ps2_data,
      ps2_int
      );

  keyboard_1 : keyboard port map
    ( clk,
      ps2_data,
      ps2_int,
      key_extended,
      key_up,
      scan_code,
      key_int
      );

  simple_7seg_1 : simple_7seg port map
    ( slow_clk,
      value_7seg,
      point_7seg,
      seg,
      dp,
      an
      );

  process (clk)
    variable count : integer range 0 to 65535 := 0;
    variable tmp_std : std_logic_vector ( 15 downto 0 );
  begin
    if rising_edge(clk) then
      count := count + 1;
    end if;
    tmp_std := conv_std_logic_vector(count,16);
    slow_clk <= tmp_std(15);
  end process;

end architecture;

