----------------------------------------------------------------------------------
-- Create Date: 16/01/2010
-- Module Name: example_driver_7segs
-- Authors    : X. Lagorce
-- Description: Example for the 7 segments display
--              This file shows the use of the driver_7seg and decoder_7seg
--                components.
--
-- Dependencies: driver_7seg, decoder_7seg, div_clk
--
-- Revision:
-- Revision 0.1 : First implementation
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity example_driver_7segs is
  port( clk  : in  STD_LOGIC;                     -- refresh clock
        a    : out STD_LOGIC;                     -- segments
        b    : out STD_LOGIC;                     --  "
        c    : out STD_LOGIC;                     --  "
        d    : out STD_LOGIC;                     --  "
        e    : out STD_LOGIC;                     --  "
        f    : out STD_LOGIC;                     --  "
        g    : out STD_LOGIC;                     --  "
        dp   : out STD_LOGIC;                     -- decimal point
        an0  : out STD_LOGIC;                     -- number selection
        an1  : out STD_LOGIC;                     --  "
        an2  : out STD_LOGIC;                     --  "
        an3  : out STD_LOGIC;                     --  "
        btn  : in  STD_LOGIC_VECTOR(3 downto 0);
        sw   : in  STD_LOGIC_VECTOR(7 downto 0);
        led  : out STD_LOGIC_VECTOR(7 downto 0)
        );
end example_driver_7segs;

architecture Behavioral of example_driver_7segs is
-- Components declarations
  component driver_7seg is
    port( clk  : in  STD_LOGIC;                     -- refresh clock
          data : in  STD_LOGIC_VECTOR(7 downto 0);  -- segments + decimal point
          add  : in  STD_LOGIC_VECTOR(1 downto 0);  -- segment address
          dok  : in  STD_LOGIC;                     -- data OK
          oe   : in  STD_LOGIC;                     -- output enable
          a    : out STD_LOGIC;                     -- segments
          b    : out STD_LOGIC;                     --  "
          c    : out STD_LOGIC;                     --  "
          d    : out STD_LOGIC;                     --  "
          e    : out STD_LOGIC;                     --  "
          f    : out STD_LOGIC;                     --  "
          g    : out STD_LOGIC;                     --  "
          dp   : out STD_LOGIC;                     -- decimal point
          an0  : out STD_LOGIC;                     -- number selection
          an1  : out STD_LOGIC;                     --  "
          an2  : out STD_LOGIC;                     --  "
          an3  : out STD_LOGIC                      --  "
          );
  end component;
  component div_clk is
    Port (clkin   :  in  STD_LOGIC;
          reset   :  in  STD_LOGIC;
          clkout1 : out  STD_LOGIC;  -- 1.49  Hz
          clkout2 : out  STD_LOGIC;  -- 763   Hz
          clkout3 : out  STD_LOGIC;  -- 98   kHz
          clkout4 : out  STD_LOGIC); -- 1.56 MHz
  end component;
  component decoder_7seg is
    port(val : in  std_logic_vector(3 downto 0);
         pti : in  std_logic;
         a   : out std_logic;
         b   : out std_logic;
         c   : out std_logic;
         d   : out std_logic;
         e   : out std_logic;
         f   : out std_logic;
         g   : out std_logic;
         pt  : out std_logic
         );
  end component;
  for all : driver_7seg use entity work.driver_7seg(Behavioral);
  for all : div_clk use entity work.div_clk(Behavioral);
  for all : decoder_7seg use entity work.decoder_7seg(hexa_dec);
-- Signals declarations
  signal data : std_logic_vector(7 downto 0);
  signal add : std_logic_vector(1 downto 0);
  signal val : std_logic_vector(3 downto 0);
  signal dok, oe, reset, clk1, clk2, clk3, clk4 : std_logic;
  signal ad, bd, cd, dd, ed, ffd, gd, ptd, pti : std_logic;
-- Description
begin
  -- Components declarations
  -- Let's get some slower clocks
  div : div_clk port map (clk, reset, clk1, clk2, clk3, clk4);
  -- The display driver
  driver : driver_7seg port map (clk2, data, add, dok, oe, a, b, c,
                                d, e, f, g, dp, an0, an1, an2, an3);
  -- component to decode BCD to 7 segments pattern
  decoder : decoder_7seg port map (val, pti, ad, bd, cd, dd, ed,
                                   ffd, gd, ptd);
  -- Internal signals assignations
  data(7) <= ad;                        -- vectorisation of the segments signals
  data(6) <= bd;
  data(5) <= cd;
  data(4) <= dd;
  data(3) <= ed;
  data(2) <= ffd;                       -- fd is a keywork to add a Verilog
                                        -- component in a VHDL design
  data(1) <= gd;
  data(0) <= ptd;

  val <= sw(3 downto 0);                -- enter a value on these switches
  oe <= sw(4);                          -- this switch enables the output
  pti <= sw(5);                         -- Decimal point

  led(3 downto 0) <= val;               -- display the value on LEDs
  led(4) <= clk1;                       -- "I'm alive !" blinky
  led(5) <= dok;                        -- Data OK signal
  led(7 downto 6) <= add;               -- Addressed display

  reset <= '0';                         -- Never reset the clock divider
  
  -- Loading values into the driver
  -- This process generates the address accordingly to the switch pressed
  process (btn)
  begin
    case btn is
      when "0001" => add <= "00";
                     dok <= '1';
      when "0010" => add <= "01";
                     dok <= '1';
      when "0100" => add <= "10";
                     dok <= '1';
      when "1000" => add <= "11";
                     dok <= '1';
      when others => add <= "00";
                     dok <= '0';
    end case;
  end process;
end Behavioral;
