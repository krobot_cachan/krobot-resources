#!/bin/sh

# File containing the firmware to flash in the board
firmware=/usr/local/lib/firmware/nexys2-firmware.bin

bus=`cat /sys/bus/usb/devices/$1/busnum`
bus=`printf "%03d" $bus`
dev=`cat /sys/bus/usb/devices/$1/devnum`
dev=`printf "%03d" $dev`
usbfile="/dev/bus/usb/$bus/$dev"

/sbin/fxload -t fx2 -D $usbfile -I $firmware >> /tmp/fpga-nexys2
