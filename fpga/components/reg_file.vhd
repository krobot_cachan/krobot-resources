----------------------------------------------------------------------------------
-- Company: 
-- Engineer: O. Bichler
-- 
-- Create Date:    18:27:15 01/18/2010 
-- Design Name: 
-- Module Name:    reg_file - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reg_file is
    Port ( clk : in  STD_LOGIC;
           raddr0 : in  STD_LOGIC_VECTOR (4 downto 0);
           rdata0 : out  STD_LOGIC_VECTOR (31 downto 0);
           raddr1 : in  STD_LOGIC_VECTOR (4 downto 0);
           rdata1 : out  STD_LOGIC_VECTOR (31 downto 0);
           wen_p : in  STD_LOGIC;
           waddr_p : in  STD_LOGIC_VECTOR (4 downto 0);
           wdata_p : in  STD_LOGIC_VECTOR (31 downto 0));
end reg_file;

architecture Behavioral of reg_file is
    type reg_array is array(integer range <>) of STD_LOGIC_VECTOR (wdata_p'range);
    signal reg: reg_array(0 to 2**waddr_p'length-1);
    
begin
    -- Combinational read ports
    rdata0 <= reg(TO_INTEGER(UNSIGNED(raddr0)));
    rdata1 <= reg(TO_INTEGER(UNSIGNED(raddr1)));
    
    -- Write port is active only when wen_p is asserted
    process(clk)
    begin
        if clk'event and clk='1' then
            if wen_p='1' then
                reg(TO_INTEGER(UNSIGNED(waddr_p))) <= wdata_p;
            end if;
        end if;
    end process;

end Behavioral;

