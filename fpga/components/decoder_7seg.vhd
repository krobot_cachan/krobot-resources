----------------------------------------------------------------------------------
-- Create Date: 15/01/2010
-- Module Name: decoder_7seg
-- Authors    : X. Lagorce
-- Description: Decodes a BCD value into its representation on a 7 segments
--              diplay
--
-- Dependencies: /
--
-- Revision:
-- Revision 0.1 : First implementation
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity decoder_7seg is
  port(val : in  std_logic_vector(3 downto 0);  -- BCD value to decode
       pti : in  std_logic;                     -- Decimal point
       a   : out std_logic;                     -- Decoded segment
       b   : out std_logic;                     --  "
       c   : out std_logic;                     --  "
       d   : out std_logic;                     --  "
       e   : out std_logic;                     --  "
       f   : out std_logic;                     --  "
       g   : out std_logic;                     --  "
       pt  : out std_logic                      --  "
       );
end decoder_7seg;

architecture hexa_dec of decoder_7seg is
begin
  process(val, pti)
  begin
    case val is
      when "0000" =>  a <= '0'; -- 0
                      b <= '0';
                      c <= '0';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '1';
      when "0001" =>  a <= '1'; -- 1
                      b <= '0';
                      c <= '0';
                      d <= '1';
                      e <= '1';
                      f <= '1';
                      g <= '1';
      when "0010" =>  a <= '0'; -- 2
                      b <= '0';
                      c <= '1';
                      d <= '0';
                      e <= '0';
                      f <= '1';
                      g <= '0';
      when "0011" =>  a <= '0'; -- 3
                      b <= '0';
                      c <= '0';
                      d <= '0';
                      e <= '1';
                      f <= '1';
                      g <= '0';
      when "0100" =>  a <= '1'; -- 4
                      b <= '0';
                      c <= '0';
                      d <= '1';
                      e <= '1';
                      f <= '0';
                      g <= '0';
      when "0101" =>  a <= '0'; -- 5
                      b <= '1';
                      c <= '0';
                      d <= '0';
                      e <= '1';
                      f <= '0';
                      g <= '0';
      when "0110" =>  a <= '0'; -- 6
                      b <= '1';
                      c <= '0';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when "0111" =>  a <= '0'; -- 7
                      b <= '0';
                      c <= '0';
                      d <= '1';
                      e <= '1';
                      f <= '1';
                      g <= '1';
      when "1000" =>  a <= '0'; -- 8
                      b <= '0';
                      c <= '0';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when "1001" =>  a <= '0'; -- 9
                      b <= '0';
                      c <= '0';
                      d <= '0';
                      e <= '1';
                      f <= '0';
                      g <= '0';
      when "1010" =>  a <= '0'; -- A
                      b <= '0';
                      c <= '0';
                      d <= '1';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when "1011" =>  a <= '1'; -- B
                      b <= '1';
                      c <= '0';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when "1100" =>  a <= '0'; -- C
                      b <= '1';
                      c <= '1';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '1';
      when "1101" =>  a <= '1'; -- D
                      b <= '0';
                      c <= '0';
                      d <= '0';
                      e <= '0';
                      f <= '1';
                      g <= '0';
      when "1110" =>  a <= '0'; -- E
                      b <= '1';
                      c <= '1';
                      d <= '0';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when "1111" =>  a <= '0'; -- F
                      b <= '1';
                      c <= '1';
                      d <= '1';
                      e <= '0';
                      f <= '0';
                      g <= '0';
      when others =>  a <= '1'; -- shouldn't happen
                      b <= '1';
                      c <= '1';
                      d <= '1';
                      e <= '1';
                      f <= '1';
                      g <= '1';
    end case;
  end process;
  pt <= pti;                            -- don't need to decode the decimal point
end hexa_dec;
