library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity mouse is
    port (clk : in std_logic;
          data_in : in std_logic_vector ( 0 to 7 );
          int_in : in std_logic;

          left_button : out std_logic;
          right_button : out std_logic;
          x_pos : out std_logic_vector ( 8 downto 0 );
          y_pos : out std_logic_vector ( 8 downto 0 );
          int : out std_logic );
end entity mouse;

architecture mouse of mouse is
  type state_type is ( waiting, bit1, bit2, end_state );
begin

process (clk)
  variable state : state_type := end_state;
  variable x_pos_v : std_logic_vector ( 7 downto 0 );
  variable y_pos_v : std_logic_vector ( 7 downto 0 );
  variable data_v : std_logic_vector ( 7 downto 0 );
  variable old_int_in : std_logic := '0';
begin
  if(rising_edge(clk))
  then
    case state is
      when end_state =>
        state := waiting;
        int <= '0';
      when waiting =>
        if(int_in = '1' and old_int_in = '0')
        then
          state := bit1;
          data_v := data_in;
        end if;
      when bit1 =>
        if(int_in = '1' and old_int_in = '0')
        then
          state := bit2;
          x_pos_v := data_in;
        end if;
      when bit2 =>
        if(int_in = '1' and old_int_in = '0')
        then
          state := end_state;
          y_pos_v := data_in;
          int <= '1';
        end if;        
    end case;

    old_int_in := int_in;
  end if;

  -- I should handle overflow and the bits seams to be in the wrong order
  x_pos <= data_v(4)&x_pos_v;  -- that should be converted to signed integer
  y_pos <= data_v(5)&y_pos_v;
  left_button <= data_v(0);
  right_button <= data_v(1);
end process;

end mouse;
