----------------------------------------------------------------------------------
-- Create Date: 15/01/2010
-- Module Name: driver_7seg
-- Authors    : X. Lagorce
-- Description: Driver for an output composed of four 7 segments displays
--              multiplexed
--
-- Dependencies: mux8_4
--
-- Revision:
-- Revision 0.1 : First implementation
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity driver_7seg is
  port( clk  : in  STD_LOGIC;                     -- refresh clock
        data : in  STD_LOGIC_VECTOR(7 downto 0);  -- segments + decimal point
        add  : in  STD_LOGIC_VECTOR(1 downto 0);  -- segment address
        dok  : in  STD_LOGIC;                     -- data OK
        oe   : in  STD_LOGIC;                     -- output enable
        a    : out STD_LOGIC;                     -- segments
        b    : out STD_LOGIC;                     --  "
        c    : out STD_LOGIC;                     --  "
        d    : out STD_LOGIC;                     --  "
        e    : out STD_LOGIC;                     --  "
        f    : out STD_LOGIC;                     --  "
        g    : out STD_LOGIC;                     --  "
        dp   : out STD_LOGIC;                     -- decimal point
        an0  : out STD_LOGIC;                     -- number selection
        an1  : out STD_LOGIC;                     --  "
        an2  : out STD_LOGIC;                     --  "
        an3  : out STD_LOGIC                      --  "
      );
end driver_7seg;

architecture Behavioral of driver_7seg is
-- Components declarations
component mux8_4 is
  Port ( c : in  STD_LOGIC_VECTOR (1 downto 0);
         e0 : in  STD_LOGIC_VECTOR (7 downto 0);
         e1 : in  STD_LOGIC_VECTOR (7 downto 0);
         e2 : in  STD_LOGIC_VECTOR (7 downto 0);
         e3 : in  STD_LOGIC_VECTOR (7 downto 0);
         s : out  STD_LOGIC_VECTOR (7 downto 0));
end component;
for all : mux8_4 use entity work.mux8_4(Behavioral);
-- Signals declarations
signal val1 : std_logic_vector(7 downto 0);
signal val2 : std_logic_vector(7 downto 0);
signal val3 : std_logic_vector(7 downto 0);
signal val4 : std_logic_vector(7 downto 0);
signal display_state : std_logic_vector(1 downto 0);
signal output : std_logic_vector(7 downto 0);
-- Description
begin
  -- Multiplexer to link the internal memories to the display
  display_mux : mux8_4 port map(display_state, val1, val2, val3, val4, output);
  -- Link output of the multiplexer to the output lines going to the display
  a  <= output(7);
  b  <= output(6);
  c  <= output(5);
  d  <= output(4);
  e  <= output(3);
  f  <= output(2);
  g  <= output(1);
  dp <= output(0);
  -- Display scanning
  process (clk)
  begin
    if (clk'event and clk = '1') then
      if (oe = '1') then                -- let's refresh if output is enabled
        case display_state is
          when "00" => display_state <= "01";
                       an0 <= '1';
                       an1 <= '0';
                       an2 <= '1';
                       an3 <= '1';
          when "01" => display_state <= "10";
                       an0 <= '1';
                       an1 <= '1';
                       an2 <= '0';
                       an3 <= '1';
          when "10" => display_state <= "11";
                       an0 <= '1';
                       an1 <= '1';
                       an2 <= '1';
                       an3 <= '0';
          when "11" => display_state <= "00";
                       an0 <= '0';
                       an1 <= '1';
                       an2 <= '1';
                       an3 <= '1';
          when others => display_state <= "00";
                       an0 <= '0';
                       an1 <= '1';
                       an2 <= '1';
                       an3 <= '1';
        end case;
      else
        an0 <= '1';                     -- don't refresh if output disabled
        an1 <= '1';
        an2 <= '1';
        an3 <= '1';
      end if;
    end if;
  end process;
  -- Manage the loading of the internal memory
  process (dok)
  begin
    if (dok'event and dok='1') then
      case add is
        when "00" => val1 <= data;
        when "01" => val2 <= data;
        when "10" => val3 <= data;
        when "11" => val4 <= data;
        when others => val1 <= data;    -- shouldn't happen
      end case;
    end if;
  end process;
end Behavioral;
