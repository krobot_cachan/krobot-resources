library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity keyboard is
    port (clk : in std_logic;
          data_in : in std_logic_vector ( 0 to 7 );
          int_in : in std_logic;

          extended : out std_logic;
          up : out std_logic;
          code : out std_logic_vector ( 0 to 7 );
          int : out std_logic );
end entity keyboard;

architecture keyboard of keyboard is
  type state_type is ( waiting, end_state );
  signal tmp_extended, tmp_up : std_logic;
begin

process (clk)
  variable state : state_type := end_state;
  variable old_int_in : std_logic := '0';
begin
  if(rising_edge(clk))
  then
    case state is
      when end_state =>
        state := waiting;
        int <= '0';
        tmp_extended <= '0';
        tmp_up <= '0';
      when waiting =>
        if(int_in = '1' and old_int_in = '0')
        then
          case data_in is 
            when "11100000" =>
              tmp_extended <= '1';
              state := waiting;
            when "11110000" =>
              tmp_up <= '1';
              state := waiting;
            when others =>
              code <= data_in;
              extended <= tmp_extended;
              up <= tmp_up;
              state := end_state;
              int <= '1';
          end case;
        end if;
    end case;

    old_int_in := int_in;
  end if;
end process;

end keyboard;
