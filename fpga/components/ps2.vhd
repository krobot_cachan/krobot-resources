library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity driver_ps2 is
    port (clk : in std_logic;
          ps2d : in std_logic;
          ps2c : in std_logic;
          data : out std_logic_vector ( 0 to 7 );
          int : out std_logic;
          -- only for debugging purpose
          st : out std_logic_vector ( 4 downto 0 ) );
end entity driver_ps2;

architecture driver_ps2 of driver_ps2 is
type state_type is ( idle, bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7,
                     bit8, bit9, end_state );
signal tmp : std_logic_vector ( 9 downto 0 ) := "0000000000";
signal state : state_type := idle;
signal got : std_logic := '0';
signal int_l : std_logic := '0';
signal data_l : std_logic_vector ( 0 to 7 ) := "00000000";
signal ps2c_old : std_logic := '1';
begin

int <= int_l;
data <= data_l;

process (clk)
begin
  if (rising_edge(clk))
  then
    ps2c_old <= ps2c;
    if (( state = idle ) and ( ps2d = '0' ))
    then
      state <= bit0;
    end if;
    if (( ps2c_old = '1' ) and ( ps2c = '0' ))
    then
      case state is
        when bit0 =>
          state <=  bit1;
          tmp(0) <= ps2d;
        when bit1 =>
          state <=  bit2;
          tmp(1) <= ps2d;
        when bit2 =>
          state <=  bit3;
          tmp(2) <= ps2d;
        when bit3 =>
          state <=  bit4;
          tmp(3) <= ps2d;
        when bit4 =>
          state <=  bit5;
          tmp(4) <= ps2d;
        when bit5 =>
          state <=  bit6;
          tmp(5) <= ps2d;
        when bit6 =>
          state <=  bit7;
          tmp(6) <= ps2d;
        when bit7 =>
          state <=  bit8;
          tmp(7) <= ps2d;
        when bit8 =>
          state <= bit9;
          tmp(8) <= ps2d;
        when bit9 =>
          state <= end_state;
          tmp(9) <= ps2d;
          int_l <= '1';
          data_l <= tmp(8 downto 1);
        when others =>
          state <= idle;
          int_l <= '0';
      end case;
    end if;
    
    case state is
      when idle =>
        st <= ps2c&"0001";
      when bit0 =>
        st <= ps2c&"0010";
      when bit1 =>
        st <= ps2c&"0011";
      when bit2 =>
        st <= ps2c&"0100";
      when bit3 =>
        st <= ps2c&"0101";
      when bit4 =>
        st <= ps2c&"0111";
      when bit5 =>
        st <= ps2c&"1000";
      when bit6 =>
        st <= ps2c&"1001";
      when bit7 =>
        st <= ps2c&"1010";
      when bit8 =>
        st <= ps2c&"1011";
      when bit9 =>
        st <= ps2c&"1100";
      when others =>
        st <= ps2c&"1101";
    end case;
  end if;
end process;

end driver_ps2;
