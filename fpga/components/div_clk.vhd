----------------------------------------------------------------------------------
-- Create Date: 15/01/2010 
-- Module Name: div_clk - Behavioral
-- Authors    : X. Lagorce
-- Description: Clock divider with multiple outputs
--
-- Dependencies: /
--
-- Revision: 
-- Revision 0.1 : Simple implementation
-- Additional Comments: This should use integrated clock circuits
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity div_clk is
  Port (clkin   :  in  STD_LOGIC;
        reset   :  in  STD_LOGIC;
        clkout1 : out  STD_LOGIC;  -- 1.49  Hz
        clkout2 : out  STD_LOGIC;  -- 763   Hz
        clkout3 : out  STD_LOGIC;  -- 98   kHz
        clkout4 : out  STD_LOGIC); -- 1.56 MHz
end div_clk;

architecture Behavioral of div_clk is
  signal compt : std_logic_vector(26 downto 0);
begin
  process (clkin, reset)
  begin
    if (reset = '1') then
      compt <= "000000000000000000000000000";
    elsif (clkin'event and clkin = '1') then
      compt <= compt + 1;
    end if;
  end process;
  clkout1 <= compt(26);
  clkout2 <= compt(17);
  clkout3 <= compt(11);
  clkout4 <= compt(6);
end Behavioral;
