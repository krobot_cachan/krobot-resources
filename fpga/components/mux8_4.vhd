----------------------------------------------------------------------------------
-- Create Date: 15/01/2010
-- Module Name: mux8_4
-- Authors    : X. Lagorce
-- Description: multiplexer 4 inputs of 8 bits to one output
--
-- Dependencies: /
--
-- Revision:
-- Revision 0.1 : First implementation
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mux8_4 is
  Port ( c : in  STD_LOGIC_VECTOR (1 downto 0);
         e0 : in  STD_LOGIC_VECTOR (7 downto 0);
         e1 : in  STD_LOGIC_VECTOR (7 downto 0);
         e2 : in  STD_LOGIC_VECTOR (7 downto 0);
         e3 : in  STD_LOGIC_VECTOR (7 downto 0);
         s : out  STD_LOGIC_VECTOR (7 downto 0));
end mux8_4;

architecture Behavioral of mux8_4 is
begin
  process(e0, e1, e2, e3, c)
  begin
    case c is
      when "00" => s <= e0;
      when "01" => s <= e1;
      when "10" => s <= e2;
      when "11" => s <= e3;
    when others => s <= "00000000";     -- shouldn't happen
    end case;
  end process;
end Behavioral;

