
entity root2 is
    port (btn  : in  bit;
          led  : out bit;
          btn2  : in  bit;
          led2  : out bit );
end entity root2;

architecture behavior2 of root2 is
component assign is
    port (yop  : in  bit;
          youp  : out bit );
end component;
for all : assign use entity work.assign(behavior);  
begin
  assign1 : assign port map (btn,led);
  assign2 : assign port map (btn2,led2);
end behavior2;
