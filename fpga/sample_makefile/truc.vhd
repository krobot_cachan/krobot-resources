
entity assign is
    port (yop  : in  bit;
          youp  : out bit );
end entity assign;

architecture behavior of assign is
begin
  youp <= yop;
end behavior;
